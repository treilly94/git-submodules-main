# git submodules main

This project is a demo of how we could use git submodules for config as code

The idea is that there is some generic config in [git-submodules-submodule1](https://gitlab.com/treilly94/git-submodules-submodule1)
would be versioned and shared across multaple environmnets. Each of the environments would then have its own config repo
(Like [git-submodules-main](https://gitlab.com/treilly94/git-submodules-main)) that would extend a specific version of the generic repo
and add env specific detail. Changes can then be moved through envs by changing the version of the generic repo each specific repo uses.

## Tags

I tried specifying a Tag to use using the `branch` field fo the `.gitmodules` file like the below
```
[submodule "git-submodules-submodule1"]
	path = git-submodules-submodule1
	url = git@gitlab.com:treilly94/git-submodules-submodule1.git
	branch = v2
```
But there seem to be some [ongoing issues](https://github.com/fmtlib/fmt/issues/412) around that. Tags can currently be used by doing
a gitcheckout within the submodule, then commiting that as a change to the main repo.

Branches can succesfully be defined in the `.gitmodules` file though
